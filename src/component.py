class Component:
    def __init__(self, model, brand, component_type, manufacturing_date, stock, price):
        self.model = model
        self.brand = brand
        self.component_type = component_type
        self.manufacturing_date = manufacturing_date
        self.stock = stock
        self.price = price

    def modify_stock(self, new_stock):
        self.stock = new_stock

    def toString(self):
        return "Model: " + self.model + ", brand: " + self.brand + ", component_type: " + self.component_type + ", manufacturing_date" + self.manufacturing_date + ", stock: " + self.stock + ", price: " + self.price
