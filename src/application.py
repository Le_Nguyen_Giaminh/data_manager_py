import time
from component import Component

inventory = {}
i = 0

def list_inventory(inv):
    for key, value in inv.items():
        print(key, ':', value.toString())


def start():
    global i
    global inventory
    print("\n-----Welcome to Data Manager-----")
    while True:
        print("\n--------")
        print("What do you want to do ?")

        action = input("Enter a number :\n1 - Add new component\n2 - Update component's stock\n3 - Delete component\n4 - List all components\n5 - Quit\n")   
        
        while(int(action)>6 or int(action)<1):
            print("\nThat's not in the proposed numbers\n")
            action = input("Enter a number :\n1 - Add new component\n2 - Update component's stock\n3 - Delete component\n4 - List all components\n5 - Quit\n")   

        action = int(action)

        if action == 1:
            model = input("Please enter the component´s model: ")
            brand = input("Please enter the component´s brand: ")
            component_type = input("Please enter the component´s type: ")
            manufacturing_date = input("Please enter the component´s manufacturing date: ")
            stock = input("Please enter the component´s stock: ")
            price = input("Please enter the component´s price: ")
            component = Component(model,brand,component_type,manufacturing_date,stock,price)
            inventory[i+1] = component
            i+=1
            time.sleep(1)
            print("\n-----\nThe component has been added to the inventory with the ID " + str(i))

        elif action == 2:
            print("\nThis is our inventory at the moment: \n")
            list_inventory(inventory)
            idC = input("\nEnter the ID of the component: ")
            idC = int(idC)
            new_stock = input("Enter the new stock: ")
            inventory[idC].modify_stock(new_stock)
            print("\n------\nThe stock has been modifed\n")
            list_inventory(inventory)
            
        elif action == 3:
            print("\nThis is our inventory at the moment: \n")
            list_inventory(inventory)
            idC = input("\nEnter the ID of the component that you want to delete: ")
            idC = int(idC)
            inventory.pop(idC)
            print("\n------\nThe component has been deleted. This is the new inventory\n")
            list_inventory(inventory)

        elif action == 4:
            print("\nThis is our inventory at the moment: \n")
            list_inventory(inventory)

        else:   
            print("\nBye bye")
            break



